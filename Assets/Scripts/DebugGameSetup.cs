﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DebugGameSetup : MonoBehaviour {

    public TextMeshProUGUI surveyCode;
    public TextMeshProUGUI playerAmount;
    public TextMeshProUGUI playersSize;
    public TextMeshProUGUI players;

    void Start()
    {
        surveyCode.text = "surveyCode: " + GameManager.Instance.surveyCode;
        playerAmount.text = "playerAmount: " + GameManager.Instance.playerAmount.ToString();
        playersSize.text = "playersSize: " + GameManager.Instance.players.Count.ToString();

        string playerNames = "";

        foreach (Player player in GameManager.Instance.players) {
            playerNames += " - " + player.name + " " + player.score + "\n";
        }

        players.text = playerNames;
    }

}