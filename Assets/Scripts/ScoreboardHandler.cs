﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreboardHandler : MonoBehaviour
{
    public GameObject scoreboardContainer;
    public GameObject scoreboardRowPrefab;

    public bool isDebug = false;
    public List<Player> dummyPlayers;

    // Start is called before the first frame update
    void Start()
    {
        List<Player> players;

        if (!isDebug)
        {
            players = GameManager.Instance.GetListRanking();
        } else {
            players = dummyPlayers;

        }

        foreach (Player player in players)
        {
            var row = Instantiate(scoreboardRowPrefab, scoreboardContainer.transform);
            var textobject = row.GetComponentsInChildren<TextMeshProUGUI>();

            textobject[0].text = player.name;
            textobject[1].text = player.score.ToString();
        }
    }
}
