﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GameManager : MonoBehaviour {

    public static GameManager Instance { get; private set; }

    public int playerAmount;
    public string surveyCode;
    public Player MVP;
    public List<Player> players;
    public bool isDebug = false;
    public int round = 0;

    private void Awake()
    {

        // Create the first (and only) instance
        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        } 
        
        // Destroy new instances immediately
        else {
            Destroy(gameObject);
        }
    }

    public void IncreaseRound()
    {
        // Increase the round number when asking a new question
        ++round;
    }

    public List<Player> GetListRanking()
    {
        return players.OrderByDescending(p => p.score).ThenBy(p => p.name).ToList();
    }

}
