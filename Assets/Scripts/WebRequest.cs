﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

public class WebRequest : MonoBehaviour
{

  private const string URL = "https://survey-api.cloudpotato.nl/api/survey";

  void Start()
  {
    StartCoroutine(PostRequest());
  }

  private IEnumerator PostRequest()
  {

    // Request Body Generation
    string surveyCode = GameManager.Instance.surveyCode;
    string rounds = GameManager.Instance.round.ToString();
    JSONPlayer[] ArrayPlayers = getPlayerArray();
    JSONQuestion[] ArrayQuestions = getQuestionsArray();

    string requestBody = JsonHelper.GenerateRequestBody(surveyCode, rounds, ArrayPlayers, ArrayQuestions, true);

    // Do new Request
    var request = new UnityWebRequest(URL, "POST");
    byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(requestBody);
    request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
    request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
    request.SetRequestHeader("Content-Type", "application/json");

    yield return request.Send();
  }

  private JSONPlayer[] getPlayerArray()
  {
    int i = 0;
    JSONPlayer[] players = new JSONPlayer[GameManager.Instance.players.Count];

    foreach (Player player in GameManager.Instance.players)
    {
      players[i] = new JSONPlayer(player.name, player.score);
      i++;
    }

    return players;
  }

  private JSONQuestion[] getQuestionsArray()
  {
    int i = 0;
    JSONQuestion[] questions = new JSONQuestion[SurveyManager.Instance.GetAnsweredQuestions().Count];

    foreach (Question question in SurveyManager.Instance.GetAnsweredQuestions())
    {
      questions[i] = new JSONQuestion(question.questionId, question.question, question.answerId, question.answer, question.description);
      Debug.Log(questions[i].question);
      i++;
    }

    return questions;
  }

}
