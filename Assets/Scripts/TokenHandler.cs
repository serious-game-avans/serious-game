﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TokenHandler : MonoBehaviour {

    public TMP_InputField surveyCodeInputField;
    public Button playGameButton;

    void Update() {
        if (surveyCodeInputField.text != "") {
            playGameButton.interactable = true;
        } else {
            playGameButton.interactable = false;
        }
    }
}
