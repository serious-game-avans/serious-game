using System;

[Serializable]
public class JSONPlayer
{

  public string nickname;
  public int score;

  public JSONPlayer(string nickname, int score)
  {
    this.nickname = nickname;
    this.score = score;
  }
}