using System;

[Serializable]
public class JSONQuestion
{

  public string questionCode;
  public string question;
  public string answerCode;
  public string answer;
  public string description;

  public JSONQuestion(string questionCode, string question, string answerCode, string answer, string description)
  {
    this.questionCode = questionCode;
    this.question = question;
    this.answerCode = answerCode;
    this.answer = answer;
    this.description = description;
  }
}