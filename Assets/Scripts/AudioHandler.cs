﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioHandler : MonoBehaviour
{
    public AudioClip backgroundMusic;

    private void Start() {
        AudioManager.Instance.PlayMusic(backgroundMusic);
    }

}
