using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class MVPHandler : MonoBehaviour
{
    public List<string> playersnames = new List<string>();
    public Dropdown dropdown;
    public Player selected_player;

    void Start()
    {
        List<Player> playersRanking = GameManager.Instance.GetListRanking();
        GameObject round = GameObject.FindGameObjectWithTag("Round");

        // Add all players
        foreach (Player player in GameManager.Instance.players)
        {
            playersnames.Add(player.name);
        }
        
        dropdown.AddOptions(playersnames);

        // Round Text
        TextMeshProUGUI roundText = round.GetComponent<TextMeshProUGUI>();
        roundText.text = "Round " + GameManager.Instance.round.ToString();

        // Scoring Text
        GameObject[] playerRankingInput = GameObject.FindGameObjectsWithTag("PlayerRanking");

        for (int c = 0; c < 3; c++)
        {
            TextMeshProUGUI text = playerRankingInput[c].GetComponent<TextMeshProUGUI>();
            Player player = playersRanking[c];
            text.text = player.name + "\n" + player.score;
        }
    }

    public void DropDownEventhandler(int index)
    {
        GameManager.Instance.MVP = GameManager.Instance.players[index - 1];
    }
}
