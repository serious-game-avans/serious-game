﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class SceneHandler : MonoBehaviour {
    private int nextSceneToLoad;
    private int previousSceneToLoad;
    public Animator animator;
    public TMP_InputField surveyCodeInputField;
    public TMP_InputField playerAmountInputField;
    public TMP_InputField questionDescriptionInputField;

    private void Start() 
    {
        nextSceneToLoad = SceneManager.GetActiveScene().buildIndex + 1;
        previousSceneToLoad = SceneManager.GetActiveScene().buildIndex - 1;
    }

    public void SubmitSurveyCode() 
    {
        GameManager.Instance.surveyCode = surveyCodeInputField.text;
        fadeNextScene();
    }

    public void SubmitPlayerAmount() 
    {
        GameManager.Instance.playerAmount = int.Parse(playerAmountInputField.text);
        fadeNextScene();
    }

    public void StartNextRound()
    {
        // TODO: Fade out
        ToScene(3);
    }

    public void SubmitPlayerNames() 
    {
        // List to set, not initialised in the GameManager!
        List<Player> players = new List<Player>();

        // Find all Text Inputs
        GameObject[] inputs = GameObject.FindGameObjectsWithTag("Input");

        // Add the text of every input to the list
        foreach (GameObject input in inputs) {
            string inputText = input.GetComponent<TMP_InputField>().text;

            if (inputText == "") inputText = input.GetComponent<TMP_InputField>().placeholder.GetComponent<TextMeshProUGUI>().text;

            players.Add(new Player(inputText));
        }

        // Set the list and navigate.
        GameManager.Instance.players = players;
        fadeNextScene();
    }

    public void SubmitQuestionAnswer()
    {
        TextMeshProUGUI sentiment = GameObject.Find("Survey_SentimentResult").GetComponent<TextMeshProUGUI>();
        TextMeshProUGUI sentimentPosPerc = GameObject.Find("Survey_SentimentPosPercentage").GetComponent<TextMeshProUGUI>();
        TextMeshProUGUI sentimentNegPerc = GameObject.Find("Survey_SentimentNegPercentage").GetComponent<TextMeshProUGUI>();

        string inputText = questionDescriptionInputField.text;

        Question question = SurveyManager.Instance.currentQuestion;

        question.answer = SurveyManager.Instance.currentAnswer;
        question.answerId = SurveyManager.Instance.currentAnswerId;

        if (inputText == null || inputText == "") {
            question.description = "";
        } else {
            question.description = inputText;
        }

        question.sentiment = sentiment.text;
        question.sentimentPosPerc = float.Parse(sentimentPosPerc.text);
        question.sentimentNegPerc = float.Parse(sentimentNegPerc.text);
        SurveyManager.Instance.SetQuestionAnsweredToTrue(question.questionId);

        fadeNextScene();
    }

    public void SubmitMVP()
    {   
        Player player = GameManager.Instance.MVP;
        player.increase();
        GameManager.Instance.MVP = null;

        fadeNextScene();
    }

    public void SubmitQrQuestion()
    {
        fadeNextScene();
    }

    public void Quit()
    {
        fadeNextScene();
    }

    private void fadeNextScene() {
        // Calls nextScene, see the Animation Editor for details.
        animator.SetTrigger("FadeOut");
    }

    public void nextScene() {
        SceneManager.LoadScene(nextSceneToLoad);
    }

    public void previousScene() {
        SceneManager.LoadScene(previousSceneToLoad);
    }

    public void ToScene(int index) {
        SceneManager.LoadScene(index);
    }
}
