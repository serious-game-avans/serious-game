﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SurveyQuestionHandler : MonoBehaviour
{
    public List<Player> playersRanking;
    public List<Question> questions;

    void Start()
    {
        questions = SurveyManager.Instance.GetUnansweredQuestions();
        playersRanking = GameManager.Instance.GetListRanking();

        // Get objects we need from view
        GameObject[] playerRankingInput = GameObject.FindGameObjectsWithTag("PlayerRanking");
        GameObject questionInput = GameObject.FindGameObjectWithTag("Question");
        GameObject round = GameObject.FindGameObjectWithTag("Round");

        /* keep track of score */
        for (int c = 0; c < 3; c++)
        {
            TextMeshProUGUI text = playerRankingInput[c].GetComponent<TextMeshProUGUI>();
            Player player = playersRanking[c];

            text.text = player.name + "\n" + player.score;
        }

        // Question Text
        TextMeshProUGUI questionText = questionInput.GetComponent<TextMeshProUGUI>();
        questionText.text = SurveyManager.Instance.currentQuestion.question;

        // Round Text
        TextMeshProUGUI roundText = round.GetComponent<TextMeshProUGUI>();
        roundText.text = "Round " + GameManager.Instance.round.ToString();
    }
}
