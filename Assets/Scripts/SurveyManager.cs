using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SurveyManager : MonoBehaviour {

    public static SurveyManager Instance { get; private set; }

    public string surveyCode;
    public List<Question> questions = new List<Question>();
    public Question currentQuestion;
    public string currentAnswer;
    public string currentAnswerId;

    private void Awake() {

        // Create the first (and only) instance
        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        } 
        
        // Destroy new instances immediately
        else {
            Destroy(gameObject);
        }
    }

    public List<Question> GetUnansweredQuestions()
    {
        return questions.FindAll(q => q.answered == false);
    }

        public List<Question> GetAnsweredQuestions()
    {
        return questions.FindAll(q => q.answered == true);
    }

    public void SetQuestionAnsweredToTrue(string questionId)
    {
        questions.Find(q => q.questionId == questionId).SetQuestionToAnswered();
    }

}
