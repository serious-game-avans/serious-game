﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


[Serializable]
public class Player
{
    public string name;
    public int score;

    public Player(string name)
    {
        this.name = name;
        this.score = 0;
    }

    public void increase()
    {
        this.score += 3;
    }
}
