﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SurveyStartRoundHandler : MonoBehaviour
{
    public List<Player> playersRanking;

    void Start()
    {
        playersRanking = GameManager.Instance.GetListRanking();
        GameManager.Instance.IncreaseRound();

        // Get objects we need from view
        GameObject[] playerRankingInput = GameObject.FindGameObjectsWithTag("PlayerRanking");
        GameObject round = GameObject.FindGameObjectWithTag("Round");

        /* keep track of score */
        for (int c = 0; c < 3; c++)
        {
            TextMeshProUGUI text = playerRankingInput[c].GetComponent<TextMeshProUGUI>();
            Player player = playersRanking[c];

            text.text = player.name + "\n" + player.score;
        }

        // Round Text
        TextMeshProUGUI roundText = round.GetComponent<TextMeshProUGUI>();
        roundText.text = "Round " + GameManager.Instance.round.ToString();
    }
}
