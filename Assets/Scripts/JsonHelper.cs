﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.Networking;
using UnityEngine;

public static class JsonHelper
{

  public static string GenerateRequestBody(string surveyCode, string rounds, JSONPlayer[] players, JSONQuestion[] questions, bool prettyPrint)
  {
    Wrapper wrapper = new Wrapper();
    wrapper.surveyCode = surveyCode;
    wrapper.rounds = rounds;
    wrapper.players = players;
    wrapper.questions = questions;
    return JsonUtility.ToJson(wrapper, prettyPrint);
  }

  [Serializable]
  private class Wrapper
  {
    public string surveyCode;
    public string rounds;
    public JSONQuestion[] questions;
    public JSONPlayer[] players;
  }

}