using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TeleType : MonoBehaviour
{
    private TextMeshProUGUI  m_textMeshPro;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        m_textMeshPro = gameObject.GetComponent<TextMeshProUGUI>() ?? gameObject.AddComponent<TextMeshProUGUI>();
        m_textMeshPro.ForceMeshUpdate();

        int totalVisibleCharacters = m_textMeshPro.textInfo.characterCount;
        int counter = 0;

        while (true)
        {
            int visibleCount = counter % (totalVisibleCharacters + 1);

            m_textMeshPro.maxVisibleCharacters = visibleCount;

            if (visibleCount >= totalVisibleCharacters)
            {
                int visibleCounter = 1;
                int amountOfBlinking = 3;
                int blinked = 0;

                while (blinked < amountOfBlinking)
                {
                    if (visibleCounter % 2 == 1)
                    {
                        m_textMeshPro.enabled = false;
                        visibleCounter += 1;
                        yield return new WaitForSeconds(0.2f);
                    }

                    if (visibleCounter % 2 == 0)
                    {
                        m_textMeshPro.enabled = true;
                        visibleCounter += 1;
                        blinked += 1;
                        yield return new WaitForSeconds(0.2f);
                    }
                }

                yield return new WaitForSeconds(1.0f);
            }

            counter += 1;

            yield return new WaitForSeconds(0.2f);
        }
    }
}
