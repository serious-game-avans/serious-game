﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerNumberHandler : MonoBehaviour
{
    public TMP_InputField playerAmountInputField;
    public Button playerAmountAddButton;
    public Button playerAmountMinusButton;

    private int players = 4;

    private void Update() {
        playerAmountInputField.text = players.ToString();

        // TODO: fix this with a ternary?
        if (players == 1) {
            setEnabled(playerAmountMinusButton, false);
        } else if (players == 8) {
            setEnabled(playerAmountAddButton, false);
        } else {
            setEnabled(playerAmountMinusButton, true);
            setEnabled(playerAmountAddButton, true);
        }
    }

    private void setEnabled(Button button, bool enabled) {
        button.interactable = enabled;
    }

    public void addPlayer() {
        if (players < 8) {
            ++players;
        }
    }

    public void subtractPlayer() {
        if (players > 0) {
            --players;
        }
    }

}
