﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnitySentiment;

public class SurveyAnswerHandler : MonoBehaviour
{
  public AudioClip trombone;
  public AudioClip applause;
  public AudioClip wow;

  public List<Player> playersRanking;

  public SentimentAnalysis predictionObject;
  public TMP_InputField answerInput;

  private bool responseFromThread = false;
  private bool threadStarted = false;
  private Vector3 SentimentAnalysisResponse;

  private TextMeshProUGUI sentiment;
  private TextMeshProUGUI sentimentPosPerc;
  private TextMeshProUGUI sentimentNegPerc;

  void Start()
  {
    playersRanking = GameManager.Instance.GetListRanking();
    GameObject[] playerRankingInput = GameObject.FindGameObjectsWithTag("PlayerRanking");
    GameObject questionInput = GameObject.FindGameObjectWithTag("Question");
    GameObject answerInput = GameObject.FindGameObjectWithTag("Answer");
    GameObject round = GameObject.FindGameObjectWithTag("Round");

    sentiment = GameObject.Find("Survey_SentimentResult").GetComponent<TextMeshProUGUI>();
    sentimentPosPerc = GameObject.Find("Survey_SentimentPosPercentage").GetComponent<TextMeshProUGUI>();
    sentimentNegPerc = GameObject.Find("Survey_SentimentNegPercentage").GetComponent<TextMeshProUGUI>();

    // Scoring Text
    for (int c = 0; c < 3; c++)
    {
      TextMeshProUGUI text = playerRankingInput[c].GetComponent<TextMeshProUGUI>();
      Player player = playersRanking[c];
      text.text = player.name + "\n" + player.score;
    }

    // Question Text
    TextMeshProUGUI questionText = questionInput.GetComponent<TextMeshProUGUI>();
    questionText.text = SurveyManager.Instance.currentQuestion.question;

    // Answer Text
    TextMeshProUGUI answerText = answerInput.GetComponent<TextMeshProUGUI>();
    answerText.text = SurveyManager.Instance.currentAnswer;

    // Round Text
    TextMeshProUGUI roundText = round.GetComponent<TextMeshProUGUI>();
    roundText.text = "Round " + GameManager.Instance.round.ToString();
  }

  void OnEnable()
  {
    Application.runInBackground = true;
    // Initialize the local database
    predictionObject.Initialize();
    // Listen to the Events
    // Sentiment analysis response
    SentimentAnalysis.OnAnlysisFinished += GetAnalysisFromThread;
    // Error response
    SentimentAnalysis.OnErrorOccurs += Errors;
  }

  void OnDestroy()
  {
    // Unload Listeners
    SentimentAnalysis.OnAnlysisFinished -= GetAnalysisFromThread;
    SentimentAnalysis.OnErrorOccurs -= Errors;
  }

  public void SendPredictionText()
  {
    // Thread-safe computations
    predictionObject.PredictSentimentText(answerInput.text);

    if (!threadStarted)
    {// Thread Started
      threadStarted = true;
      StartCoroutine(WaitResponseFromThread());
    }
  }

  // Sentiment Analysis Thread
  private void GetAnalysisFromThread(Vector3 analysisResult)
  {
    SentimentAnalysisResponse = analysisResult;
    responseFromThread = true;
    //trick to call method to the main Thread
  }

  private IEnumerator WaitResponseFromThread()
  {
    while (!responseFromThread) // Waiting For the response
    {
      yield return null;
    }
    // Main Thread Action
    PrintAnalysis();
    // Reset
    responseFromThread = false;
    threadStarted = false;
  }

  private void PrintAnalysis()
  {
    if (SentimentAnalysisResponse.x > SentimentAnalysisResponse.y && SentimentAnalysisResponse.x > SentimentAnalysisResponse.z)
    {
      AudioManager.Instance.PlaySFX(applause);
      sentiment.text = "Positive";
      sentimentNegPerc.text = SentimentAnalysisResponse.y.ToString();
      sentimentPosPerc.text = SentimentAnalysisResponse.x.ToString();
    }
    else if (SentimentAnalysisResponse.y > SentimentAnalysisResponse.x && SentimentAnalysisResponse.y > SentimentAnalysisResponse.z)
    {
      AudioManager.Instance.PlaySFX(trombone);
      sentiment.text = "Negative";
      sentimentNegPerc.text = SentimentAnalysisResponse.y.ToString();
      sentimentPosPerc.text = SentimentAnalysisResponse.x.ToString();
    }
    else if (SentimentAnalysisResponse.z > SentimentAnalysisResponse.x && SentimentAnalysisResponse.z > SentimentAnalysisResponse.y)
    {
      AudioManager.Instance.PlaySFX(wow);
      sentiment.text = "Neutral";
      sentimentNegPerc.text = SentimentAnalysisResponse.y.ToString();
      sentimentPosPerc.text = SentimentAnalysisResponse.x.ToString();
    }
  }

  // Sentiment Analysis Thread
  private void Errors(int errorCode, string errorMessage)
  {
    Debug.Log(errorMessage + "\nCode: " + errorCode);
  }
}
