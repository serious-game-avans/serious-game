using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public class Question
{
  public string questionId;
  public string question;
  public string answer;
  public string answerId;
  public string description;
  public bool answered;
  public string sentiment;
  public float sentimentPosPerc;
  public float sentimentNegPerc;

  public Question(string questionId, string question)
  {
    this.questionId = questionId;
    this.question = question;
    this.answerId = "";
    this.answer = "";
    this.description = "";
    this.answered = false;
  }

  public void SetQuestionToAnswered()
  {
    this.answered = true;
  }
}