﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerNamesHandler : MonoBehaviour {
    
    void Start() {
        GameObject[] inputs = GameObject.FindGameObjectsWithTag("Input");

        int playerAmount = GameManager.Instance.playerAmount;
        int index = 0;

        foreach (GameObject input in inputs) {
            TMP_InputField textField = input.GetComponent<TMP_InputField>();
            if (index >= playerAmount) input.SetActive(false);
            ++index;
        }
    }
}
