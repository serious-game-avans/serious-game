﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class QRCodeHandler : MonoBehaviour
{
  public static QRCodeHandler Instance { get; private set; }

  public Dictionary<string, string> qrAnswersList = new Dictionary<string, string>();
  public Dictionary<string, string> qrQuestionsList = new Dictionary<string, string>();

  public string FindAnswer(string qrCode)
  {
    return qrAnswersList[qrCode];
  }

  public string FindQuestion(string qrCode)
  {
    return qrQuestionsList[qrCode];
  }

  public string RandomAnswer()
  {
    System.Random rnd = new System.Random();
    string answer = qrAnswersList.ElementAt(rnd.Next(0, qrAnswersList.Count)).Value;
    string answerId = qrAnswersList.ElementAt(rnd.Next(0, qrAnswersList.Count)).Key;

    SurveyManager.Instance.currentAnswerId = answerId;

    return answer;
  }

  public Question RandomQuestion()
  {
    System.Random rnd = new System.Random();
    int value = rnd.Next(0, qrQuestionsList.Count);
    return new Question("SQ" + value, qrQuestionsList.ElementAt(value).Value);
  }

  private void Start()
  {
    importAnswersList();
    importQuestionsList();
  }

  private void importQuestionsList()
  {
    qrQuestionsList.Add("SQ01", "Welk gevoel roept het afgelopen blok bij je op?");
    qrQuestionsList.Add("SQ02", "Tijdens dit blok was ____ mijn ergste frustratie.");
    qrQuestionsList.Add("SQ03", "Dit blok heeft mij erg ____ gemaakt.");
    qrQuestionsList.Add("SQ04", "Door ____ situatie kwam ik tot mijn hoogtepunt dit blok.");
    qrQuestionsList.Add("SQ05", "Creatief Denken");
    qrQuestionsList.Add("SQ06", "Door ____ wilde ik naar huis.");
    qrQuestionsList.Add("SQ07", "Wanneer ____ in de factory was wilde ik ____.");
    qrQuestionsList.Add("SQ08", "Dit blok heeft mij niet klaargestoomd voor ____.");
    qrQuestionsList.Add("SQ09", "Ik heb nog nooit zo veel ____ als in dit blok.");
    qrQuestionsList.Add("SQ10", "Dit blok heb ik het meest genoten van ____.");
    qrQuestionsList.Add("SQ11", "Samenwerken maakt mij ____.");
    qrQuestionsList.Add("SQ12", "____ is hoe ik de band met mijn leraren omschrijf.");
    qrQuestionsList.Add("SQ13", "Wanneer het vak ____ is, snap ik er de ballen niet van.");
    qrQuestionsList.Add("SQ14", "Op tijd beginnen voor dit blok is ____.");
    qrQuestionsList.Add("SQ15", "____ Hadden we ook online kunnen doen.");
    qrQuestionsList.Add("SQ16", "Dit blok is een ____ blok.");
    qrQuestionsList.Add("SQ17", "Ik was zo voorbereid voor mijn tentamens dat ____.");
    qrQuestionsList.Add("SQ18", "____ heeft mijn psyche beschadigd.");
    qrQuestionsList.Add("SQ19", "Werken voor een opdrachtgever ____.");
    qrQuestionsList.Add("SQ20", "Wat was er teveel dit blok?");
    qrQuestionsList.Add("SQ21", "Wat was er te weinig dit blok?");
    qrQuestionsList.Add("SQ22", "Hulp van ____ kan ik beter niet vragen.");
    qrQuestionsList.Add("SQ23", "Door ____ ontwikkelde ik een intense relatie met mijn laptop.");
    qrQuestionsList.Add("SQ24", "Als ____ niet ophoudt met de onzin over ____ schrijf ik me nu nog uit.");
    qrQuestionsList.Add("SQ25", "Ik raad andere opleidingen ____ aan om te proberen.");
    qrQuestionsList.Add("SQ26", "Om meer plezier te hebben op school raad ik aan om ____.");
    qrQuestionsList.Add("SQ27", "____ heeft zijn imago weer echt nageleefd.");
    qrQuestionsList.Add("SQ28", "In de volgende aflevering van leven in de factory ____.");
    qrQuestionsList.Add("SQ29", "Wat is de grootste kracht van deze opleiding?");
    qrQuestionsList.Add("SQ30", "Wat is de grootste kracht van dit blok?");
    qrQuestionsList.Add("SQ31", "Mijn grootste kracht binnen deze opleiding is?");
    qrQuestionsList.Add("SQ32", "Wanneer ik blokcoördinator ben van de opleiding business innovation zou ik ____.");
    qrQuestionsList.Add("SQ33", "____ verdiend een eigen standbeeld.");
    qrQuestionsList.Add("SQ34", "Van ____ naar ____ in dit blok.");
    qrQuestionsList.Add("SQ35", "Wanneer ik bezig was met brainstormen gebeurde ____.");
    qrQuestionsList.Add("SQ36", "Voor mijn gevoel vinden veel bedrijven onze opleiding ____.");
    qrQuestionsList.Add("SQ37", "De stage was voor mij ____.");
    qrQuestionsList.Add("SQ38", "____ verlaagt de waarde van ons diploma.");
    qrQuestionsList.Add("SQ39", "Punt Avans nieuws-update!! Business innovation studenten ____.");
    qrQuestionsList.Add("SQ40", "De concepten die ik dit blok bedacht heb waren echt ____.");
    qrQuestionsList.Add("SQ41", "Vakken als ____ zijn geweldig.");
    qrQuestionsList.Add("SQ42", "Momenten als ____ zijn geweldig.");
    qrQuestionsList.Add("SQ43", "Ik krijg nog steeds maagkrampen als ik terugdenk aan ____.");
    qrQuestionsList.Add("SQ44", "____ zou verboden moeten worden!");
    qrQuestionsList.Add("SQ45", "Wat is een BI student zijn favoriete bezigheid?");
    qrQuestionsList.Add("SQ46", "Waarom bestaat ____?");
    qrQuestionsList.Add("SQ47", "Ik neem ____ mee naar volgend studiejaar.");
    qrQuestionsList.Add("SQ48", "Ik voel mezelf zo slim door ____.");
    qrQuestionsList.Add("SQ49", "____ neem ik nooit serieus.");
    qrQuestionsList.Add("SQ51", "Zeg het hardop! Ik ben ____ en ik ben er trots op!");
    qrQuestionsList.Add("SQ52", "____ houd alsjeblieft eeeeens keer op met praten!");
    qrQuestionsList.Add("SQ53", "SLB lessen.");
    qrQuestionsList.Add("SQ54", "De blok borrels waren dit blok ____.");
    qrQuestionsList.Add("SQ55", "I got 99 problems, but ____ ain't one.");
    qrQuestionsList.Add("SQ56", "I got 99 problems, but ____ ain't one.");
    qrQuestionsList.Add("SQ57", "Tijdens de blok aftrap ____.");
    qrQuestionsList.Add("SQ58", "Medestudenten, voor dit blok voorbij is hebben we ____.");
    qrQuestionsList.Add("SQ59", "Alleen drank had ____ leuker kunnen maken.");
    qrQuestionsList.Add("SQ60", "Een leuk blok zou onmogelijk zijn zonder ____.");
    qrQuestionsList.Add("SQ61", "____ heeft me enorm geholpen.");
    qrQuestionsList.Add("SQ62", "Wat beëindigde jouw vorige relatie?");
  }

  private void importAnswersList()
  {
    qrAnswersList.Add("SA1", "Organisatie en Marketing");
    qrAnswersList.Add("SA2", "Creative Tools");
    qrAnswersList.Add("SA3", "Kritisch Denken");
    qrAnswersList.Add("SA4", "Marketing en Consumentgedrag");
    qrAnswersList.Add("SA5", "Design 1 Communicatieontwerp");
    qrAnswersList.Add("SA6", "Branding 1 Merkactivatie");
    qrAnswersList.Add("SA7", "Communicatie");
    qrAnswersList.Add("SA8", "Scenario Planning");
    qrAnswersList.Add("SA9", "Business Cases");
    qrAnswersList.Add("SA10", "Stage");
    qrAnswersList.Add("SA11", "Keuze Modules");
    qrAnswersList.Add("SA12", "Creatief Denken");
    qrAnswersList.Add("SA13", "Project");
    qrAnswersList.Add("SA14", "SLB");
    qrAnswersList.Add("SA15", "PGOZ");
    qrAnswersList.Add("SA16", "Faciliteren");
    qrAnswersList.Add("SA17", "Persoonlijke Vaardigheden");
    qrAnswersList.Add("SA18", "Engels");
    qrAnswersList.Add("SA19", "Innovatie");
    qrAnswersList.Add("SA20", "Onderzoek");
    qrAnswersList.Add("SA21", "Ard Jacobs");
    qrAnswersList.Add("SA22", "Eric van Gennip");
    qrAnswersList.Add("SA23", "David van Dinther");
    qrAnswersList.Add("SA24", "Anneloes van Prooijen");
    qrAnswersList.Add("SA25", "Pail Lelkes");
    qrAnswersList.Add("SA26", "Janneke Brouwers");
    qrAnswersList.Add("SA27", "Roel Steendijk");
    qrAnswersList.Add("SA28", "Joris Funcke");
    qrAnswersList.Add("SA29", "Sven Coevers");
    qrAnswersList.Add("SA30", "Adriaan Wagenaars");
    qrAnswersList.Add("SA31", "Rene van der Burgt");
    qrAnswersList.Add("SA32", "Maartje Damen");
    qrAnswersList.Add("SA33", "Yvonne Koert");
    qrAnswersList.Add("SA34", "Jan Spruijt");
    qrAnswersList.Add("SA35", "Marc Vonk");
    qrAnswersList.Add("SA36", "Carla bakker");
    qrAnswersList.Add("SA37", "Dood van binnen");
    qrAnswersList.Add("SA38", "Zeer gelukkig");
    qrAnswersList.Add("SA39", "Als Leonardo Dicaprio in een romantische komedie");
    qrAnswersList.Add("SA40", "Twijfel");
    qrAnswersList.Add("SA41", "Gestress(ed)");
    qrAnswersList.Add("SA42", "Zo lam als een versleten hond");
    qrAnswersList.Add("SA43", "Toch een beetje balen");
    qrAnswersList.Add("SA44", "Verveeld");
    qrAnswersList.Add("SA45", "Als het vinden van een nummertje die je al maanden lang kent");
    qrAnswersList.Add("SA46", "Alsof ik een stoeprand heb gekopt");
    qrAnswersList.Add("SA47", "Overreden door een vrachtwagen");
    qrAnswersList.Add("SA48", "Als een zeemeeuw die net een frietje heeft kunnen ");
    qrAnswersList.Add("SA49", "Als trump als hij over Mexico denkt");
    qrAnswersList.Add("SA50", "Ge-wel-dig");
    qrAnswersList.Add("SA51", "Het vinden van de deuren naar de hel");
    qrAnswersList.Add("SA52", "Als het terugkomen van een toch net iets te goedkope kapper");
    qrAnswersList.Add("SA53", "Rapper Sjors die net pindakaas heeft gegeten");
    qrAnswersList.Add("SA54", "Niks");
    qrAnswersList.Add("SA55", "Feest");
    qrAnswersList.Add("SA56", "Als de ochtend na een blokborrel");
    qrAnswersList.Add("SA57", "Als 100 rijden op de snelweg na 19:00");
    qrAnswersList.Add("SA58", "Een hamburger laten vallen");
    qrAnswersList.Add("SA59", "Het net zien wegrijden van je bus/trein wanneer je het perron op komt");
    qrAnswersList.Add("SA60", "Lopen aan de linkerkant op de trap");
    qrAnswersList.Add("SA61", "Poets vrouw op het Avans");
    qrAnswersList.Add("SA62", "Kurwa");
    qrAnswersList.Add("SA63", "Het faken van een brainstorm");
    qrAnswersList.Add("SA64", "Laat ze het zelf doen die opdrachtgevers");
    qrAnswersList.Add("SA65", "Vraag mij is wat ik erom geef");
    qrAnswersList.Add("SA66", "Alsof een engeltje over mijn tong piest");
    qrAnswersList.Add("SA67", "Als een happy end in een massage salon");
    qrAnswersList.Add("SA68", "Het inkicken van de ket op een afterparty");
    qrAnswersList.Add("SA69", "Erg vervelend");
    qrAnswersList.Add("SA70", "Koffie");
    qrAnswersList.Add("SA71", "Broodje subway");
    qrAnswersList.Add("SA72", "Een pen");
    qrAnswersList.Add("SA73", "Laptop");
    qrAnswersList.Add("SA74", "Oplader");
    qrAnswersList.Add("SA75", "Broodje unox");
    qrAnswersList.Add("SA76", "Pils");
    qrAnswersList.Add("SA77", "Handvat van een deur");
    qrAnswersList.Add("SA78", "Het gaspedaal van een Lamborghini");
    qrAnswersList.Add("SA79", "Kiwi");
    qrAnswersList.Add("SA80", "Heggenschaar");
    qrAnswersList.Add("SA81", "Touw met een knoop");
    qrAnswersList.Add("SA82", "Open grasveld");
    qrAnswersList.Add("SA83", "Uitgedroogd meer in Zuid Amerika");
    qrAnswersList.Add("SA84", "Katy perry");
    qrAnswersList.Add("SA85", "Gerard Joling");
    qrAnswersList.Add("SA86", "Factory");
    qrAnswersList.Add("SA87", "Makerspace");
    qrAnswersList.Add("SA88", "Online werkomgeving");
    qrAnswersList.Add("SA89", "Werkdruk");
    qrAnswersList.Add("SA90", "De twitter van Donald Trump");
    qrAnswersList.Add("SA91", "De Email van Hillary Clinton");
    qrAnswersList.Add("SA92", "Een koptelefoon opzetten");
    qrAnswersList.Add("SA93", "Een bom erop AUB");
    qrAnswersList.Add("SA94", "Een zee van problemen");
    qrAnswersList.Add("SA95", "Het hebben van grote dromen");
    qrAnswersList.Add("SA96", "Een bezoekje aan de kantine");
    qrAnswersList.Add("SA97", "Water halen");
    qrAnswersList.Add("SA98", "Een zeikwijf");
    qrAnswersList.Add("SA99", "Mijn overleden goudvis");
    qrAnswersList.Add("SA100", "De trappen van het avans");
    qrAnswersList.Add("SA101", "Kippetjes bekijken in de gangen");
    qrAnswersList.Add("SA102", "Thuisbezorgd bestellen in de factory");
    qrAnswersList.Add("SA103", "Het tonen van emotie");
    qrAnswersList.Add("SA104", "Het inleveren van een deadline op 23:59");
    qrAnswersList.Add("SA105", "Het hamsteren van wc-papier");
    qrAnswersList.Add("SA106", "Nachtje doorhalen voor een deadline");
    qrAnswersList.Add("SA107", "Pilzen in de factory");
    qrAnswersList.Add("SA108", "Business Innovator");
    qrAnswersList.Add("SA109", "Constant aangestaard worden");
    qrAnswersList.Add("SA110", "Kotsen na een avond uitgaan");
    qrAnswersList.Add("SA111", "Doen alsof school leuk is");
    qrAnswersList.Add("SA112", "Een kruisiging");
    qrAnswersList.Add("SA113", "Mijn stalker");
    qrAnswersList.Add("SA114", "Mishandeling");
    qrAnswersList.Add("SA115", "Onverwachte zwangerschap");
    qrAnswersList.Add("SA116", "Lekker thuis blijven");
    qrAnswersList.Add("SA117", "SDG S");
    qrAnswersList.Add("SA118", "Kern eigenschap");
    qrAnswersList.Add("SA119", "Iemand die roept: horen jullie mij?");
  }

  private void Awake()
  {

    // Create the first (and only) instance
    if (Instance == null)
    {
      Instance = this;
      DontDestroyOnLoad(gameObject);
    }

    // Destroy new instances immediately
    else
    {
      Destroy(gameObject);
    }
  }
}
