﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    private static AudioManager instance;
    private AudioSource musicSource;
    private AudioSource sfxSource;

    public static AudioManager Instance {
        get {
            if (instance == null) {
                instance = FindObjectOfType<AudioManager>();
                if (instance == null) {
                    instance = new GameObject("AudioManager Started", typeof(AudioManager)).GetComponent<AudioManager>();
                }
            }
            return instance;
        }
        private set {
            instance = value;
        }
    }

    private void Awake() {
        DontDestroyOnLoad(this.gameObject);
        musicSource = this.gameObject.AddComponent<AudioSource>();
        sfxSource = this.gameObject.AddComponent<AudioSource>();

        musicSource.loop = true;
    }

    public void PlayMusic(AudioClip musicClip) {
        musicSource.clip = musicClip;
        musicSource.volume = 0.5f;
        musicSource.Play();
    }

    public void PlaySFX(AudioClip clip)
    {
        sfxSource.clip = clip;
        sfxSource.Play();
    }
    
}
